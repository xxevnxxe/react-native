import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import LoginPage from './components/LoginPage';
import DashboardPage from './components/DashboardPage';
import DataListsPage from './components/DataListsPage';
import UserPage from './components/UserPage';
import ProfilePage from './components/ProfilePage';

const Drawer = createDrawerNavigator();

const LoginStack = ({navigation}) => {
  return <LoginPage navigation={navigation} />;
};

const DashboardStack = ({navigation}) => {
  return <DashboardPage navigation={navigation} />;
};

const DataListsPageStack = ({navigation}) => {
  return <DataListsPage navigation={navigation} />;
};

const UserPageStack = ({navigation}) => {
  return <UserPage navigation={navigation} />;
};

const ProfilePageStack = ({navigation}) => {
  return <ProfilePage navigation={navigation} />;
};

const CustomDrawer = ({props, navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'rgba(25,25,25,1)'}}>
      <DrawerContentScrollView {...props}>
        <Text
          style={{
            fontSize: 35,
            color: 'white',
            marginHorizontal: 15,
            marginTop: 100,
            fontWeight: 'bold',
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(255,255,255,0.1)',
            marginBottom: 15,
            paddingBottom: 25,
          }}>
          React Native - JSON Fetch Apps
        </Text>
        <DrawerItem
          inactiveTintColor={'rgba(255,255,255,1)'}
          label="Dashboard"
          onPress={() => {
            navigation.navigate('Dashboard');
          }}
        />
        <DrawerItem
          inactiveTintColor={'rgba(255,255,255,1)'}
          label="User"
          onPress={() => {
            navigation.navigate('User');
          }}
        />
      </DrawerContentScrollView>
    </View>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={{
          headerShown: false,
          drawerActiveBackgroundColor: 'rgba(255,255,255,0.1)',
          drawerActiveTintColor: 'rgba(255,255,255,1)',
          drawerInactiveTintColor: 'rgba(255,255,255,0.5)',
          drawerItemStyle: {
            margin: 0,
            padding: 0,
            borderRadius: 0,
          },
        }}
        drawerContent={props => <CustomDrawer {...props} />}>
        {/* <Drawer.Screen name="Login" component={LoginStack} /> */}
        <Drawer.Screen name="Dashboard" component={DashboardStack} />
        <Drawer.Screen name="DataLists" component={DataListsPageStack} />
        <Drawer.Screen name="User" component={UserPageStack} />
        <Drawer.Screen name="Profile" component={ProfilePageStack} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
