# About this repo

This repo is meant for React Native personal project. All code is free to download and to be used.

Note :
- Login page doesn't work yet (The page is available, but turned off because it hasn't yet implemented with Firebase Auth)
- If the login page doesn't work, so do the Register page (The page is removed for now)

This link below is somewhat to be the build app example :
https://drive.google.com/file/d/1RDJAvGhHTyd54jhq5q9rv-9bfngqvqDT/view?usp=sharing
