import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/dist/FontAwesome';
import {TouchableOpacity} from 'react-native-gesture-handler';

const UserPage = ({navigation}) => {
  return (
    <ScrollView style={{backgroundColor: 'rgba(255,251,239,1)', padding: 25}}>
      <View style={[styles.navbar]}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{}}>
          <Icons color={'rgba(25,25,25,1)'} size={30} name="times"></Icons>
        </TouchableOpacity>
      </View>
      <View style={{alignItems: 'flex-start'}}>
        <Text style={styles.pil}>LOGGED IN AS</Text>
      </View>
      <Text style={styles.pageTitle}>dwikialfian92@gmail.com</Text>
      <TouchableOpacity style={styles.button} activeOpacity={0.9}>
        <Text style={styles.buttonText}>Sign Out</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default UserPage;

const styles = StyleSheet.create({
  pil: {
    backgroundColor: 'rgba(25,25,25,1)',
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 15,
    fontSize: 10,
  },
  navbar: {
    backgroundColor: 'rgba(255,251,239,1)',
    flex: 0,
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginBottom: 75,
  },
  pageTitle: {
    color: 'rgba(25,25,25,1)',
    paddingBottom: 10,
    marginBottom: 50,
    marginTop: 10,
    fontSize: 25,
  },
  button: {
    backgroundColor: 'rgba(25,25,25,1)',
    flexDirection: 'column',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  buttonText: {
    color: 'rgba(255,251,239,1)',
    textAlign: 'center',
  },
});
