import {StyleSheet, Text, View, TextInput, ScrollView} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

const LoginPage = ({navigation}) => {
  return (
    <ScrollView style={[styles.lightBackground, styles.flexed, {padding: 25}]}>
      <Text style={styles.topPil}>beta 0.1</Text>
      <Text style={styles.pageTitle}>React Native Assignment Test</Text>
      <TextInput
        style={[styles.input]}
        placeholder="E-Mail"
        placeholderTextColor={'rgba(25,25,25,0.5)'}
      />
      <TextInput
        style={[styles.input, {marginTop: 5}]}
        placeholder="Password"
        placeholderTextColor={'rgba(25,25,25,0.5)'}
      />
      <View
        style={{
          borderTopColor: 'rgba(25,25,25,0.15)',
          borderTopWidth: 1,
          marginTop: 15,
          paddingTop: 15,
        }}>
        <TouchableOpacity
          onPress={() => {
            // navigation.navigate('Dashboard');
            navigation.replace('Dashboard');
          }}
          activeOpacity={0.8}>
          <Text style={styles.buttonDark}>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.8}>
          <Text style={[styles.buttonLight, {marginTop: 5}]}>
            Sign In with Google
          </Text>
        </TouchableOpacity>
        <Text style={styles.textCenter}>
          Don't have an account? Sign up now
        </Text>
      </View>
    </ScrollView>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  buttonDark: {
    backgroundColor: 'rgba(25,25,25,1)',
    padding: 15,
    textAlign: 'center',
    color: 'rgba(255,251,239,1)',
  },
  buttonLight: {
    backgroundColor: 'rgba(25,25,25,0.15)',
    padding: 15,
    textAlign: 'center',
    color: 'rgba(25,25,25,1)',
  },
  lightBackground: {
    backgroundColor: 'rgba(255,251,239,1)',
  },
  flexed: {
    flex: 1,
  },
  topPil: {
    backgroundColor: 'rgba(25,25,25,1)',
    color: 'rgba(255,255,251,1)',
    paddingVertical: 5,
    paddingHorizontal: 25,
    borderRadius: 30,
    marginLeft: 'auto',
    fontSize: 10,
  },
  pageTitle: {
    color: 'rgba(25,25,25,1)',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 50,
    marginBottom: 150,
    fontFamily: 'Krone One',
  },
  input: {
    backgroundColor: 'rgba(25,25,25,0.15)',
    color: 'rgba(25,25,25,1)',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: 'rgba(25,25,25,0.5)',
  },
  textCenter: {
    textAlign: 'center',
    color: 'rgba(25,25,25,1)',
    fontSize: 10,
    marginTop: 15,
    color: 'rgba(25,25,25,0.5)',
  },
});
