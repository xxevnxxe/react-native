import {ScrollView, StyleSheet, Text, View, Linking} from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/dist/FontAwesome';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';

const ProfilePage = ({navigation}) => {
  return (
    <ScrollView style={{backgroundColor: 'rgba(25,25,25,1)'}}>
      <View style={{padding: 25}}>
        <View style={[styles.navbar]}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Icons
              color={'rgba(255,251,239,1)'}
              size={30}
              name="chevron-left"></Icons>
          </TouchableOpacity>
        </View>
        <Text style={styles.pageTitle}>Profile</Text>
        <TouchableOpacity style={styles.card} activeOpacity={1}>
          <Text style={styles.cardPil}>Name</Text>
          <Text style={styles.cardText}>Dwiki Alfian</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} activeOpacity={1}>
          <Text style={styles.cardPil}>Hobby</Text>
          <Text style={styles.cardText}>Playing Video Games</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.card} activeOpacity={1}>
          <Text style={styles.cardPil}>Nationality</Text>
          <Text style={styles.cardText}>Indonesian</Text>
        </TouchableOpacity>
        <Text
          style={{
            color: 'rgba(255,251,239,1)',
            fontSize: 10,
            marginBottom: 10,
            marginTop: 25,
          }}>
          LINKS
        </Text>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL('https://github.com/DwikiAlfian');
          }}
          style={styles.cardBig}
          activeOpacity={0.7}>
          <Icons
            style={{marginLeft: 'auto'}}
            size={30}
            color={'rgba(25,25,25,1)'}
            name="github"></Icons>
          <Text style={styles.cardBigText}>GitHub</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL('https://instagram.com/rexothyst');
          }}
          style={styles.cardBig}
          activeOpacity={0.7}>
          <Icons
            style={{marginLeft: 'auto'}}
            size={30}
            color={'rgba(25,25,25,1)'}
            name="instagram"></Icons>
          <Text style={styles.cardBigText}>Instagram</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(
              'https://www.linkedin.com/in/dwiki-alfian-89b958171/',
            );
          }}
          style={styles.cardBig}
          activeOpacity={0.7}>
          <Icons
            style={{marginLeft: 'auto'}}
            size={30}
            color={'rgba(25,25,25,1)'}
            name="linkedin"></Icons>
          <Text style={styles.cardBigText}>LinkedIn</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default ProfilePage;

const styles = StyleSheet.create({
  navbar: {
    flex: 0,
    flexDirection: 'column',
  },
  user: {
    backgroundColor: 'rgba(25,25,25,1)',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    color: 'rgba(255,251,239,1)',
    marginTop: 30,
    marginBottom: 15,
    fontSize: 20,
  },
  card: {
    backgroundColor: 'rgba(255,251,239,1)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'flex-start',
    marginBottom: 5,
  },
  cardPil: {
    backgroundColor: 'rgba(25,25,25,1)',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 15,
    fontSize: 10,
    color: 'rgba(255,251,239,1)',
    marginRight: 10,
  },
  cardText: {
    color: 'rgba(25,25,25,1)',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,251,239,1)',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 25,
    marginTop: 25,
  },
  buttonText: {
    color: 'rgba(25,25,25,1)',
    marginRight: 15,
    fontSize: 10,
  },
  cardBig: {
    backgroundColor: 'rgba(255,251,239,1)',
    flexDirection: 'column',
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginBottom: 5,
  },
  cardBigText: {
    color: 'rgba(25,25,25,1)',
    fontSize: 22,
    marginTop: 25,
  },
});
