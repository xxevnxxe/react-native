import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/dist/FontAwesome';
import {TouchableOpacity} from 'react-native-gesture-handler';

const DashboardPage = ({navigation}) => {
  return (
    <ScrollView style={{backgroundColor: 'rgba(255,251,239,1)', padding: 25}}>
      <View style={[styles.navbar]}>
        <TouchableOpacity
          onPress={() => {
            navigation.openDrawer();
          }}>
          <Icons color={'rgba(25,25,25,1)'} size={30} name="bars"></Icons>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('User');
          }}
          style={styles.user}>
          <Icons color={'rgba(255,251,239,1)'} size={25} name="user"></Icons>
        </TouchableOpacity>
      </View>
      <Text style={styles.pageTitle}>Dashboard</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DataLists');
        }}
        style={styles.button}
        activeOpacity={0.8}>
        <Text style={styles.buttonText}>Data List</Text>
        <Icons color={'rgba(255,251,239,1)'} name="chevron-right" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Profile');
        }}
        style={[styles.button, {marginTop: 5}]}
        activeOpacity={0.8}>
        <Text style={styles.buttonText}>Profile Page</Text>
        <Icons color={'rgba(255,251,239,1)'} name="chevron-right" />
      </TouchableOpacity>
    </ScrollView>
  );
};

export default DashboardPage;

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: 'rgba(255,251,239,1)',
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  user: {
    backgroundColor: 'rgba(25,25,25,1)',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    color: 'rgba(25,25,25,1)',
    marginTop: 75,
    paddingBottom: 75,
    marginBottom: 15,
    fontSize: 35,
    borderBottomColor: 'rgba(25,25,25,0.25)',
    borderBottomWidth: 1,
  },
  button: {
    backgroundColor: 'rgba(25,25,25,1)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  buttonText: {
    color: 'rgba(255,251,239,1)',
  },
});
