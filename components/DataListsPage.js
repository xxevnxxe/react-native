import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import Icons from 'react-native-vector-icons/dist/FontAwesome';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import {useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Another from './Another';

const Stack = createNativeStackNavigator();

const DataListsPage = ({navigation}) => {
  // Data State to be Shown
  const [data, setData] = useState([
    {id: 0, title: 'No Data to be shown', body: 'Please check you connection'},
  ]);

  // Fetch Data from JSONPlaceholder
  const fetchData = async () => {
    const response = await (
      await fetch('https://jsonplaceholder.typicode.com/posts')
    ).json();
    setData(response);
  };

  // Hooks for fetching the data
  useEffect(() => {
    fetchData();
  }, []);

  // URL for delete request
  const url = 'https://jsonplaceholder.typicode.com/posts/' + id;

  // Set card's ID
  const [id, setId] = useState();

  // Fetching Request
  const deleteData = async () => {
    const response = await fetch(url, {
      method: 'DELETE',
    });
  };

  // Updating data in local state
  const filter = () => {
    setData(prevItems => {
      return prevItems.filter(item => item.id != id);
    });
  };

  // Hooks for deleting data
  useEffect(() => {
    deleteData();
    filter();
  }, [id]);

  const ListCard = ({navigation, item}) => {
    return (
      <TouchableOpacity style={styles.card} activeOpacity={0.9}>
        <Icons
          style={{marginLeft: 'auto'}}
          color={'rgba(255,251,239,1)'}
          name="trash"
          size={18}
          onPress={() => {
            setId(item.id);
          }}
        />
        <Text style={styles.cardText}>{item.title}</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Detail', {
              id: item.id,
              title: item.title,
              body: item.body,
            });
          }}
          style={styles.button}
          activeOpacity={0.8}>
          <Text style={styles.buttonText}>READ MORE</Text>
          <Icons color={'rgba(25,25,25,1)'} name="chevron-right"></Icons>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  const Lists = ({navigation}) => {
    return (
      <ScrollView style={{backgroundColor: 'rgba(255,251,239,1)', padding: 25}}>
        <View style={[styles.navbar]}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Icons
              color={'rgba(25,25,25,1)'}
              size={30}
              name="chevron-left"></Icons>
          </TouchableOpacity>
        </View>
        <Text style={styles.pageTitle}>Data Lists</Text>
        <FlatList
          data={data}
          renderItem={({item}) => (
            <ListCard navigation={navigation} item={item} />
          )}
          keyExtractor={item => item.id}
          key={item => item.id}
        />
      </ScrollView>
    );
  };

  // Data Lists Stack
  const ListsStack = ({navigation}) => {
    return <Lists navigation={navigation} />;
  };

  // Data Detail Stack
  const DetailStack = ({route, navigation}) => {
    return <Another route={route} navigation={navigation} />;
  };

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Group>
        <Stack.Screen name="Lists" component={ListsStack} />
      </Stack.Group>
      <Stack.Group screenOptions={{presentation: 'modal'}}>
        <Stack.Screen
          options={{headerShown: false}}
          name="Detail"
          component={DetailStack}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
};

export default DataListsPage;

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: 'rgba(255,251,239,1)',
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  user: {
    backgroundColor: 'rgba(25,25,25,1)',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    color: 'rgba(25,25,25,1)',
    marginTop: 75,
    paddingBottom: 10,
    marginBottom: 15,
    fontSize: 25,
    borderBottomColor: 'rgba(25,25,25,0.25)',
    borderBottomWidth: 1,
  },
  card: {
    backgroundColor: 'rgba(25,25,25,1)',
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  cardText: {
    color: 'rgba(255,251,239,1)',
    marginVertical: 15,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,251,239,1)',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 25,
    marginTop: 25,
  },
  buttonText: {
    color: 'rgba(25,25,25,1)',
    marginRight: 15,
    fontSize: 10,
  },
});
