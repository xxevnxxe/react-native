import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Icons from 'react-native-vector-icons/dist/FontAwesome';
import {TouchableOpacity} from 'react-native-gesture-handler';

const DetailPage = () => {
  return (
    <ScrollView style={{backgroundColor: 'rgba(25,25,25,1)', padding: 25}}>
      <View style={[styles.navbar]}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{marginLeft: 'auto'}}>
          <Icons color={'rgba(255,251,239,1)'} size={30} name="times"></Icons>
        </TouchableOpacity>
      </View>
      <Text style={styles.pageTitle}>sgj</Text>
      <TouchableOpacity style={styles.card} activeOpacity={1}>
        <Text style={styles.cardPil}>DESCRIPTION</Text>
        <Text style={styles.cardText}>
          quia et suscipit\nsuscipit recusandae consequuntur expedita et
          cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem
          sunt rem eveniet architecto
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default DetailPage;

const styles = StyleSheet.create({
  navbar: {
    flex: 0,
    flexDirection: 'column',
  },
  user: {
    backgroundColor: 'rgba(25,25,25,1)',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    color: 'rgba(255,251,239,1)',
    marginTop: 75,
    marginBottom: 30,
    fontSize: 35,
  },
  card: {
    backgroundColor: 'rgba(255,251,239,1)',
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  cardPil: {
    backgroundColor: 'rgba(25,25,25,1)',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 15,
    fontSize: 10,
    color: 'rgba(255,251,239,1)',
    marginBottom: 10,
  },
  cardText: {
    color: 'rgba(25,25,25,1)',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255,251,239,1)',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 25,
    marginTop: 25,
  },
  buttonText: {
    color: 'rgba(25,25,25,1)',
    marginRight: 15,
    fontSize: 10,
  },
});
